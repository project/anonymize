<?php

/**
 * @todo
 */
function anonymize_administration_mapping_form() {
  $form = array();
  $form['intro']['#markup'] = "<p>List of accepted formatters:</p>";
  $form['intro']['#markup'] .= "<ul>";
  $form['intro']['#markup'] .= "<li>Faker: <code>latitude('-90', '90')</code></li>";
  $form['intro']['#markup'] .= "<li>Faker + Tokens: <code>firstName('[user:field_gender]')</code></li>";
  $form['intro']['#markup'] .= "<li>Tokens (+ text): <code>Some text [node:field_tag:field_name].</code></li>";
  $form['intro']['#markup'] .= "<li>Clear data: <code>&lt;empty&gt;</code>.</li>";
  $form['intro']['#markup'] .= "</ul>";
  $form['intro']['#markup'] .= "<p>See <a href=\"https://github.com/fzaninotto/Faker/blob/master/readme.md\">fzaninotto/Faker (GitHub.com)</a> for formatters with Faker.</p>";

  $fields = field_info_fields();
  foreach($fields as $field) {
    $form = array_merge_recursive($form, _anonymize_admin_field_mapping($field));
  }

  $bundle_types = field_info_bundles();
  foreach ($bundle_types as $type => $bundles) {
    if (!isset($form[ANONYMIZE_VAR_PREFIX . $type])) {
      continue;
    }

    $form[ANONYMIZE_VAR_PREFIX . $type] += array(
      '#type' => 'fieldset',
      '#title' => $type,
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
    );
  }

  return system_settings_form($form);
}

/**
 * @todo
 */
function _anonymize_admin_field_mapping($field) {
  $element = array();
  $name = $field['field_name'];
  $bundle_types = $field['bundles'];
  $columns = array_keys($field['columns']);

  ctools_include('fields');
  foreach($bundle_types as $type => $bundles) {
    $var = variable_get(ANONYMIZE_VAR_PREFIX . $type);
    $value = !empty($var[$name]) ? $var[$name] : '';

    foreach($columns as $column) {
      $element[ANONYMIZE_VAR_PREFIX . $type][$name][$column] = array(
        '#title' => ctools_field_label($name) . " <code>{$name}[{$column}]</code>",
        '#description' => t('Field is used in bundles: %types', array(
            '%types' => implode(', ', $bundles),
          )
        ),
        '#type' => 'textfield',
        '#default_value' => trim($value[$column]),
      );
    }
  }

  return $element;
}
