<?php

/**
 * @file
 * Features module drush integration.
 */

/**
 * Implements hook_drush_command().
 *
 * @return
 *   An associative array describing your command(s).
 *
 * @see drush_parse_command()
 */
function anonymize_drush_command() {
  $items = array();

  $items['anonymize'] = array(
    'description' => "List all the available features for your site.",
    'options' => array(
      'type' => "Entity Type with fields to anonymize, can be 'node', 'user', 'taxonomy_term', etc",
    ),
    'drupal dependencies' => array('faker'),
    'aliases' => array('anon'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function anonymize_drush_help($section) {
  switch ($section) {
    case 'drush:anonymize':
      return dt("Anonymize field data and populate with random data.");
  }
}

/**
 * Get a list of all feature modules.
 */
function drush_anonymize() {
  $type = drush_get_option('type') ? drush_get_option('type') : NULL;

  $entity_types = is_null($type) ? entity_get_info($type) : array(entity_get_info($type));
  $run = FALSE;

  foreach ($entity_types as $entity_type => $enity_info) {
    if (!is_null($type)) {
      $entity_type = $type;
    }

    if (entity_type_is_fieldable($entity_type)) {
      $run = TRUE;
      _anonymize_entities($entity_type);
    }
  }

  if ($type && !$run) {
    return drush_set_error('', dt('@type is not valid fieldable entity type.', array('@type' => $type)));
  }
}
